import os
import math
import random as rn
import numpy as np
import logging
import h5py

import argparse
import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
import torch.optim as optim
import tqdm
import sys
import functools
import json

import pickle
import io
import boto3

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

class simpleCNN(nn.Module):
    def __init__(self):
        super(simpleCNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, 9, 2)
        self.conv2 = nn.Conv2d(10, 20, 7, 2)
        self.conv3 = nn.Conv2d(20, 35, 5, 2)
        
        self.fc1 = nn.Linear(35*42*43, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc_class = nn.Linear(64, 2)
        self.fc_range = nn.Linear(64, 1)
        
    def forward(self, x):
        
        # convolutional layers
        x = F.relu(self.conv1(x))
        #x = F.max_pool2d(x, 2)
        x = F.relu(self.conv2(x))
        #x = F.max_pool2d(x, 2)
        x = F.relu(self.conv3(x))
        #x = F.max_pool2d(x, 2)
        # flatten
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        y_class = self.fc_class(x)
        y_range = self.fc_range(x)
        
        return [y_class, y_range]

def load_h5(path):

    """
    Loads spectrogram data and labels saved in an h5 format.

    Parameters
    ----------
    path: str, path where the h5 file is saved.

    Returns
    ----------
    X: array-like, calculated spectrograms of shape (n_examples, 251, 251, 1).
    y: array-like, labels of shape (n_labels, n_examples).
    """
    print(path)
    
    with h5py.File(path, 'r') as f:
        
        X = f["data"][:]
        y = f["labels"][:]

    return X, y

class SPECT(torch.utils.data.Dataset):
    def __init__(self, X, y):
        self.X = X
        self.y = y
        
    def __getitem__(self, idx):
        
        x = torch.Tensor(self.X[idx])
        # the original x puts num channel as the last dimension 
        # torch expect the the num channel as the second dimension 
        # permuting the x to get the correct shape
        
        x = x.permute(2, 0, 1)
        
        
        # first is range, second is class
        y = torch.Tensor(self.y[idx][[0, -1]])
        
        return x, y
    
    def __len__(self):
        return self.X.shape[0]        

def _get_train_dataloader(batch_size, training_dir):
    logger.info("Get train data loader")
    # logger.info("training_dir is ".format(training_dir))
    X_train, y_train= load_h5(os.path.join(training_dir, 'grid_data_train.h5'))
    train_data = SPECT(X_train, y_train)
    train_dataloader = DataLoader(train_data,
                                  batch_size=batch_size,
                                  #num_workers=4,
                                  shuffle=True)  
    return train_dataloader

def _get_val_dataloader(batch_size, val_dir):
    logger.info("Get test data loader")
    X_val, y_val= load_h5(os.path.join('/opt/ml/input/data/eval/', 'grid_data_val.h5'))
    val_data = SPECT(X_val, y_val)
    val_dataloader = DataLoader(val_data,
                                  batch_size=batch_size,
                                  #num_workers=4,
                                  shuffle=True)  
    return val_dataloader

def train(dataloader, model, criterion_class, criterion_range, optimizer, device):
    model = model.to(device)
    model.train()
    epoch_losses_sum = []
    epoch_losses_range = []
    epoch_losses_class = []
    epoch_accs = []

    for batch in dataloader:
        dat, lab = batch
        prediction = model(dat.to(device))
        
        range_lab = lab[:, 0][:, None].to(device)
        class_lab = lab[:, 1].long().to(device)
        
        loss_class = criterion_class(prediction[0], class_lab)
        accuracy = get_accuracy(prediction[0], class_lab)
        
        loss_range = mse_loss(range_lab, prediction[1], -1., criterion_range)
        
        # sum up losses for two tasks 
        loss = loss_class + loss_range
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        epoch_losses_sum.append(loss.item())
        epoch_losses_range.append(loss_range.item())
        epoch_losses_class.append(loss_class.item())
        epoch_accs.append(accuracy.item())

    return epoch_losses_sum, epoch_losses_range, epoch_losses_class, epoch_accs

def runTrain(args):
    is_distributed = len(args.hosts) > 1 and args.backend is not None
    logger.debug("Distributed training - {}".format(is_distributed))
    use_cuda = args.num_gpus > 0
    logger.debug("Number of gpus available - {}".format(args.num_gpus))
    kwargs = {"num_workers": 1, "pin_memory": True} if use_cuda else {}
    device = torch.device("cuda" if use_cuda else "cpu")

    if is_distributed:
        # Initialize the distributed environment.
        world_size = len(args.hosts)
        os.environ["WORLD_SIZE"] = str(world_size)
        host_rank = args.hosts.index(args.current_host)
        os.environ["RANK"] = str(host_rank)
        dist.init_process_group(backend=args.backend, rank=host_rank, world_size=world_size)
        logger.info(
            "Initialized the distributed environment: '{}' backend on {} nodes. ".format(
                args.backend, dist.get_world_size()
            )
            + "Current host rank is {}. Number of gpus: {}".format(dist.get_rank(), args.num_gpus)
        )

    # set the seed for generating random numbers
    torch.manual_seed(args.seed)
    if use_cuda:
        torch.cuda.manual_seed(args.seed)

    train_loader = _get_train_dataloader(args.batch_size, args.data_dir)
    test_loader = _get_val_dataloader(args.test_batch_size, args.data_dir)

    logger.debug(
        "Processes {}/{} ({:.0f}%) of train data".format(
            len(train_loader.sampler),
            len(train_loader.dataset),
            100.0 * len(train_loader.sampler) / len(train_loader.dataset),
        )
    )

    logger.debug(
        "Processes {}/{} ({:.0f}%) of test data".format(
            len(test_loader.sampler),
            len(test_loader.dataset),
            100.0 * len(test_loader.sampler) / len(test_loader.dataset),
        )
    )

    model = simpleCNN()
#     if is_distributed and use_cuda:
#         # multi-machine multi-gpu case
#         model = torch.nn.parallel.DistributedDataParallel(model)
#     else:
#         # single-machine multi-gpu case or single-machine or multi-machine cpu case
#         model = torch.nn.DataParallel(model)

    optimizer = optim.Adam(model.parameters(), lr=args.lr)
    
    # Learning Rate Scheduler
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', patience=5)
    
    # Loss Function
    criterion_class = nn.CrossEntropyLoss().to(device)
    criterion_range = nn.MSELoss().to(device)
    best_valid_loss = float('inf')
    
    train_losses = []
    train_accs = []
    train_losses_range = []
    train_losses_class = []

    valid_losses = []
    valid_accs = []
    valid_losses_range = []
    valid_losses_class = []
    
    e_loss_train=[]
    e_loss_val=[]
    e_acc_train=[]
    e_acc_val=[]
    e_class_train=[]
    e_class_val=[]
    e_range_train=[]
    e_range_val=[]
    
   
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    for epoch in range(args.epochs):
        train_loss, train_loss_range, train_loss_class, train_acc = train(train_loader, model, 
                                           criterion_class, criterion_range, optimizer, device)
        valid_loss, valid_loss_range, valid_loss_class, valid_acc = test(test_loader, model, 
                                           criterion_class, criterion_range, device)
        # test(model, test_loader, device)

        train_losses.extend(train_loss)
        train_accs.extend(train_acc)
        train_losses_range.extend(train_loss_range)
        train_losses_class.extend(train_loss_class)

        valid_losses.extend(valid_loss)
        valid_accs.extend(valid_acc)
        valid_losses_range.extend(valid_loss_range)
        valid_losses_range.extend(valid_loss_class)

        epoch_train_loss = np.mean(train_loss)
        epoch_train_loss_range = np.mean(train_loss_range)
        epoch_train_loss_class = np.mean(train_loss_class)
        epoch_train_acc = np.mean(train_acc)

        epoch_valid_loss = np.mean(valid_loss)
        epoch_valid_loss_range = np.mean(valid_loss_range)
        epoch_valid_loss_class = np.mean(valid_loss_class)
        epoch_valid_acc = np.mean(valid_acc)
        
        
         # update learning rate 
        scheduler.step(epoch_valid_loss)
        
        
         # for plotting
        e_loss_val.append(epoch_valid_loss)
        e_loss_train.append(epoch_train_loss)
        e_acc_train.append(epoch_train_acc)
        e_acc_val.append(epoch_valid_acc)
        e_class_train.append(epoch_train_loss_class)
        e_class_val.append(epoch_valid_loss_class)
        e_range_train.append(epoch_train_loss_range)
        e_range_val.append(epoch_valid_loss_range)

        # Save the model that achieves the smallest validation loss.        
        if epoch_valid_loss < best_valid_loss:
            best_valid_loss = epoch_valid_loss
            save_model(model, args.model_dir)

        print('---')

        logger.info(f'''
        epoch: {epoch+1}
        train_loss: {epoch_train_loss:.3f},
        train_loss_range: {epoch_train_loss_range:.3f}, 
        train_loss_class: {epoch_train_loss_class:.3f},
        train_acc: {epoch_train_acc:.3f} \n
        
        valid_loss: {epoch_valid_loss:.3f},
        valid_loss_range: {epoch_valid_loss_range:.3f},
        valid_loss_class: {epoch_valid_loss_class:.3f},
        valid_acc: {epoch_valid_acc:.3f}\n
        ''')
    saveLogs(e_loss_val, 'e_loss_val')     
    saveLogs(e_loss_train, 'e_loss_train') 
    saveLogs(e_acc_train, 'e_acc_train') 
    saveLogs(e_acc_val, 'e_acc_val') 
    saveLogs(e_class_train, 'e_class_train') 
    saveLogs(e_class_val, 'e_class_val') 
    saveLogs(e_range_train, 'e_range_train') 
    saveLogs(e_range_val, 'e_range_val') 
    
    logger.info(f'''
        best_loss: {best_valid_loss:.3f};
        ''')
    
def test(dataloader, model, criterion_class, criterion_range, device):
    model.eval()
    epoch_losses_sum = []
    epoch_losses_range = []
    epoch_losses_class = []
    epoch_accs = []

    with torch.no_grad():
        for batch in dataloader:
            dat, lab = batch
            prediction = model(dat.to(device))

            range_lab = lab[:, 0][:, None].to(device)
            class_lab = lab[:, 1].long().to(device)

            loss_class = criterion_class(prediction[0], class_lab)
            accuracy = get_accuracy(prediction[0], class_lab)

            loss_range = mse_loss(range_lab, prediction[1], -1., criterion_range)

            # sum up losses for two tasks 
            loss = loss_class + loss_range
            
            epoch_losses_sum.append(loss.item())
            epoch_losses_range.append(loss_range.item())
            epoch_losses_class.append(loss_class.item())
            epoch_accs.append(accuracy.item())
        

    return epoch_losses_sum, epoch_losses_range, epoch_losses_class, epoch_accs

def get_accuracy(prediction, label):
    batch_size, _ = prediction.shape
    predicted_classes = prediction.argmax(dim=-1)
    correct_predictions = predicted_classes.eq(label).sum()
    accuracy = correct_predictions / batch_size
    return accuracy

def mse_loss(inputs, target, ignored_index, criterion):
    mask = target == ignored_index    
    out = criterion(target[~mask], inputs[~mask])
    return out

def model_fn(model_dir):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = torch.nn.DataParallel(simpleCNN())
    with open(os.path.join(model_dir, "simplecnn.pt"), "rb") as f:
        model.load_state_dict(torch.load(f))
    return model.to(device)

def saveLogs(l, name):
    s3_client = boto3.client('s3')
    array = io.BytesIO()
    loss_array = np.array(l)
    pickle.dump(loss_array, array)
    array.seek(0)
    s3_client.upload_fileobj(array, 'sounddata', 'modeling/'+name+'.pkl')

def save_model(model, model_dir):
    logger.info("Saving the model.")
    path = os.path.join(model_dir, "simplecnn_tuned_real.pt")
    # recommended way from http://pytorch.org/docs/master/notes/serialization.html
    torch.save(model.cpu().state_dict(), path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # Data and model checkpoints directories
    parser.add_argument(
        "--batch-size",
        type=int,
        default=64,
        metavar="N",
        help="input batch size for training (default: 64)",
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1000,
        metavar="N",
        help="input batch size for testing (default: 1000)",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=10,
        metavar="N",
        help="number of epochs to train (default: 10)",
    )

    parser.add_argument(
        "--lr", type=float, default=0.01, metavar="LR", help="learning rate (default: 0.01)"
    )
    parser.add_argument(
        "--momentum", type=float, default=0.5, metavar="M", help="SGD momentum (default: 0.5)"
    )
    parser.add_argument("--seed", type=int, default=1, metavar="S", help="random seed (default: 1)")
    parser.add_argument(
        "--log-interval",
        type=int,
        default=100,
        metavar="N",
        help="how many batches to wait before logging training status",
    )
    parser.add_argument(
        "--backend",
        type=str,
        default=None,
        help="backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)",
    )

    # Container environment
    parser.add_argument("--hosts", type=list, default=json.loads(os.environ["SM_HOSTS"]))
    parser.add_argument("--current-host", type=str, default=os.environ["SM_CURRENT_HOST"])
    parser.add_argument("--model-dir", type=str, default=os.environ["SM_MODEL_DIR"])
    parser.add_argument("--data-dir", type=str, default=os.environ["SM_CHANNEL_TRAINING"])
    parser.add_argument("--num-gpus", type=int, default=os.environ["SM_NUM_GPUS"])
    
    runTrain(parser.parse_args())