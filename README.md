# Right Whale Abundance

## Table of Contents

* [Introduction](#introduction)
* [Project Pipeline](#project-pipeline)
* [Modeling](#modeling)
* [Annotation Pipeline](#annotation-pipeline)
* [Deliverables](#deliverables)

## Introduction

Once widely abundant throughout the North Pacific ocean, the eastern North Pacific right whale (NPRW) population is now one of the rarest populations of marine mammals. Classified as critically endangered under the International Union for the Conservation of Nature (Wade et al. 2011), right whales have been illegally hunted for decades and are now nearing extinction. The most recent abundance estimate for this population estimated a mere 31 individuals remaining in the Bering Sea (95\% confidence estimate: 23 to 54) (Wade et al. 2010). However, this 12-year-old estimate is considered outdated and unreliable for use in conservation and marine mammal stock management, a task which our client, the National Oceanic and Atmospheric Administration (NOAA), is responsible for. Updating this population's abundance estimate with traditional methods such as mark-recapture using photographs is particularly difficult due to the population's sparsity and traditionally far-reaching habitat (Shelden et al. 2005). Through this capstone project, we were tasked with automating the range estimation process and classifying `gunshot` call type (dispersed versus non-dispersed) for this endangered population through passive acoustic data modeling. 

In this project, we illustrate our pipeline for data manipulation, label generation, and machine learning modeling using a multi-output neural network to identify (classification) and range (regression) dispersed gunshots of North Pacific right whales from passive acoustic recordings in the Bering Sea. We also present preliminary findings for use of this CNN model and how this pipeline can be used by NOAA to build a probability detection curve to be used in density estimation.

## Project Pipeline
![image](images/project_pipeline.png)

Raw data is in `wav` file format are first ingested and stored in AWS S3 to downstream visualization, labeling, and modeling purposes.

## Modeling

Since sound files are processed into spectrograms, deep learning networks that are designed for images are used to classify dispersed and non-dispersed gunshots and estimate range. Two different neural network architectures are experimented: CNN and ResNet.

![image](images/model_architecture.png)

## Annotation Pipeline 

### Label Generation - Dispersed vs Non-Dispersed Calls

[This tutorial](tutorials/RavenAnnotationTutorial.mp4) provides a high-level description of the label generation step for the classification task, which is to classify the right whale gunshot calls as dispersed and non-dispersed.The csv file contains gunshot call’s information such as start time, end time, high frequency, low frequency, and the name of the sound file that each of these gunshot calls belongs to. Since the type of these gunshot calls, whether it is a dispersed or non-dispersed, is not present, we created an annotation pipeline for the analysts to efficiently label each call signal as dispersed or non disperse call by appending that information as a column to the table. 

### Annotation Labeling Pipeline - File Structure

With Python, we created 9 folders using the mooring locations name present in the input csv file, and generated a selection table for each sound file with gunshot calls information recorded. The sound files as well as their corresponding selection tables, which are formatted as text files, are saved in their corresponding mooring location folder, and these files can be accessed using the Marine Lab virtual machine. The screenshots below demonstrate the organized file structure in the virtual machine. 

![](images/fileStructure.png)

### Annotation Labeling Pipeline - Raven Software

Using RavenLite or RavenPro, once it reads in a sound file, we will need to drag the corresponding text file and Raven will automatically generate the bounding boxes around each of the gunshot call information present in the selection table txt file, as these screenshots are showing here. 

![](images/Raven.png)

For each bounding box Raven generates, you can easily zoom in to examine the details of the specific area on the spectrogram to label the current call as dispersed or non dispersed and add it to a new column in the selection table.

## Deliverables

* [150 Warped Signals - Validation Data](Warping_Signals.xlsx)
* [Open-Source Modeling Pipeline](modeling)
* R Shiny App for Spectrogram Plotting
* [Annotation Labeling Pipeline - Raven Software](tutorials/RavenAnnotationTutorial.mp4)
* [Tutorials](tutorials)
